package com.example.examenc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.example.examenc1.R;
import com.example.examenc1.RectanguloActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextNombre, editTextBase, editTextAltura;
    private Button buttonLimpiar, buttonSiguiente, buttonSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNombre = findViewById(R.id.editTextNombre);
        editTextBase = findViewById(R.id.editTextBase);
        editTextAltura = findViewById(R.id.editTextAltura);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonSiguiente = findViewById(R.id.buttonSiguiente);
        buttonSalir = findViewById(R.id.buttonSalir);

        buttonLimpiar.setOnClickListener(v -> {
            editTextNombre.setText("");
            editTextBase.setText("");
            editTextAltura.setText("");
        });

        buttonSiguiente.setOnClickListener(v -> {
            String nombre = editTextNombre.getText().toString();
            String baseStr = editTextBase.getText().toString();
            String alturaStr = editTextAltura.getText().toString();

            if (!nombre.isEmpty() && !baseStr.isEmpty() && !alturaStr.isEmpty()) {
                try {
                    float base = Float.parseFloat(baseStr);
                    float altura = Float.parseFloat(alturaStr);

                    Intent intent = new Intent(MainActivity.this, RectanguloActivity.class);
                    intent.putExtra("nombre", nombre);
                    intent.putExtra("base", base);
                    intent.putExtra("altura", altura);
                    startActivity(intent);
                } catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "Ingrese valores numéricos válidos para base y altura", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MainActivity.this, "Por favor, llene todos los campos", Toast.LENGTH_SHORT).show();
            }
        });



        buttonSalir.setOnClickListener(v -> finish());
    }
}

